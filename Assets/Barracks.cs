﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barracks : BuildingBase {

	const float RespawnTimeout = 0f;

	public UnitScript Unit;

	void Awake () {
		Unit.UActionDeath += Respawn;
	}

	void Respawn(UnitScript unit) {
		Unit.transform.localPosition = new Vector3 (0,-0.5f,0);
		Unit.GetComponentInChildren<Renderer> ().material = TMaterials.Transparent;
	}

	void Update() {
		if (!Unit.Alive) {
			Unit.transform.localPosition += new Vector3 (0,0.4f*Time.deltaTime,0);
			if (Unit.transform.localPosition.y >= 0.5f) {
				Unit.transform.localPosition = new Vector3 (0,0.5f,0);
				Unit.GetComponentInChildren<Renderer> ().material = TMaterials.Normal;
				Unit.Revive ();
				if (Team == TeamColor.Player) {
					Unit.State = UnitState.Ready;
				}
			}
		}
	}

	public override void UpdateTeam (TeamColor team)
	{
		base.UpdateTeam (team);
		Unit.Team = team;
		Unit.OnDeath ();
	}
}
