﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallTreeScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Vector3 rotation = transform.localRotation.eulerAngles;
		rotation.y = Random.Range (0, 360);
		transform.localRotation = Quaternion.Euler(rotation);
		transform.localScale*= Random.Range (0.8f, 1.6f);
		enabled = false; //to improve performance a bit
	}
}
