﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FloaterScript : MonoBehaviour {
	
	void Start () {
		transform.DOLocalMoveY (-transform.localPosition.y, 3f).SetEase(Ease.Linear).SetLoops (-1, LoopType.Yoyo);
	}
}
