﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CapturePoint : BuildingBase {

	public Transform ProgressImage;
	public GameObject Teritory;
	public AudioClip CapturedSound;
	TeamColor CapturingTeam = TeamColor.None;
	float chargeRate = 0.2f;

	AudioSource AudioSrc;

	void Awake() {
		ProgressImage.localScale = Vector3.zero;
		AudioSrc = GetComponent<AudioSource> ();
	}

	void Update () {
		if (CapturingTeam != TeamColor.None
			&& CapturingTeam != Team
			&& (Team == TeamColor.Neutral || !GetComponentInParent<ClusterScript>().HasBuildings)
			) {
			if (!AudioSrc.isPlaying) {
				AudioSrc.Play ();
			}
			ProgressImage.localScale += Vector3.one * Time.deltaTime * chargeRate;
			AudioSrc.pitch = ProgressImage.localScale.y;
			if (ProgressImage.localScale.x >= 1f) {
				AudioSrc.Stop ();
				AudioSrc.PlayOneShot (CapturedSound);
				UpdateTeam (CapturingTeam);
				CapturingTeam = TeamColor.None;
			}

		}
	}

	public override void UpdateTeam (TeamColor team)
	{
		base.UpdateTeam (team);
		ProgressImage.localScale = Vector3.zero;
		Teritory.GetComponent<Renderer> ().material = TMaterials.Transparent;
		foreach(var building in GetComponentInParent<ClusterScript>().Buildings) {
			if (building != this) {
				building.UpdateTeam (team);
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player") && CapturingTeam == TeamColor.None && Team != other.GetComponent<HeroScript> ().Team) {
			ProgressImage.localScale = Vector3.zero;
			CapturingTeam = other.GetComponent<HeroScript> ().Team;
			other.GetComponent<HeroScript> ().CapturingCluster = GetComponentInParent<ClusterScript>();
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.CompareTag ("Player") && other.GetComponent<HeroScript>().Team == CapturingTeam) {
			CapturingTeam = TeamColor.None;
			other.GetComponent<HeroScript> ().CapturingCluster = null;
			ProgressImage.localScale = Vector3.zero;
			GetComponent<AudioSource> ().Stop ();

		}
	}
}
