﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScript : MonoBehaviour {

	public Transform Arrow;
	public Transform player;

	[Header("Tutorial Objects")]
	public CapturePoint CapturePoint1;
	public UnitScript Unit1;
	public CapturePoint CapturePoint2;
	public GameObject BuildMenu;


	// Use this for initialization
	void Start () {
		StartCoroutine (Tutorial1());
	}

	void SetArrow(Vector3 point) {
		Arrow.gameObject.SetActive (true);
		Arrow.transform.position = new Vector3 (point.x, 10, point.z);
		Arrow.LookAt (player);
		Arrow.rotation = Quaternion.Euler (0, Arrow.rotation.eulerAngles.y, 0);
	}

	void DisableArrow() {
		Arrow.gameObject.SetActive (false);
	}

	void SetDialogue(string message) {
		GameManager.Current.SetDialogue (message);
	}
	
	IEnumerator Tutorial1() {
		yield return new WaitForSeconds (1f);
		SetDialogue ("Use WASD to move.\nStand on a \"Checkpoint\" (circular platform) to capture it.");
		SetArrow (CapturePoint1.transform.position);
		yield return new WaitUntil (() => (CapturePoint1.Team == TeamColor.Player));
		DisableArrow ();
		SetDialogue ("Nice Job!\nNow you can control units.\nPress \"E\" to give command \"Follow\"");
		yield return new WaitUntil (() => (Unit1.State == UnitState.Following));
		SetDialogue ("Great!\nNow go towards enemy base, your units will follow and attack nearby buildings." +
			"\nTo capture enemy base, you first have to destroy buildings.");
		SetArrow (CapturePoint2.transform.position);
		yield return new WaitUntil (() => (Unit1.State == UnitState.Attacking));
		SetDialogue ("\"E\" = Follow me!\n\"Q\" = Stand here!\nDestroy buildings and capture enemy base.");
		yield return new WaitUntil (() => (CapturePoint2.Team == TeamColor.Player));
		GameManager.Current.Gold = 100;
		DisableArrow ();
		SetDialogue ("Left mouse button to choose building\nRight mouse button to cancel.");
		yield return new WaitUntil (() => (BuildMenu.activeSelf));
		SetDialogue ("To be continued...");
		yield return new WaitForSeconds (5f);
		SetDialogue ("");
	}
}
