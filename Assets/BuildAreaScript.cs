﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildAreaScript : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Building")) {
			other.transform.SetParent (GetComponentInParent<ClusterScript> ().transform, true);
			other.transform.GetComponent<BuildingBase> ().UpdateTeam (GetComponentInParent<CapturePoint> ().Team);
		}
	}
}
