﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderScript : MonoBehaviour {

	Vector3 Spawn;


	void Start() {
		Spawn = GameObject.FindGameObjectWithTag ("Player").transform.position;
	}

	void OnTriggerExit(Collider other)
	{
		
		if (other.CompareTag ("Player")) {
			other.transform.position = Spawn;
		}
	}
}
