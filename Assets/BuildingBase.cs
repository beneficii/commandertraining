﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingBase : MonoBehaviour {

	public GameObject BasePlatform;
	public TeamColor Team;
	protected TeamMaterials TMaterials {
		get {
			return GameManager.Current.TeamMaterials [(int)Team];
		}
	}

	public int Cost = 1;

	public void UpdateTeam() {
		UpdateTeam (Team);
	}

	public virtual void UpdateTeam (TeamColor team) {
		Team = team;
		foreach (var platform in BasePlatform.GetComponentsInChildren<Renderer> ()) {
			platform.material = TMaterials.Normal;
		}
	}
}
