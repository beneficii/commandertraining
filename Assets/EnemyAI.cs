﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AIState {
	Waiting,
	Ready,
	Traveling,
	Attacking,
	Capturing,
	Returning
}

public class EnemyAI : HeroScript {

	public ClusterScript TargetCluster;
	CapturePoint TargetPoint;
	Transform TargetBuilding = null;
	AIState State;

	Vector3 StartingPoint;

	float Speed = 3f;

	public List<UnitScript> Troops;

	void Start() {
		StartingPoint = transform.position;
		Troops = new List<UnitScript> ();
		TargetPoint = TargetCluster.Point;
		InvokeRepeating ("CheckState", 5f, 2f);
	}

	void SetVisible(bool visible) {
		GetComponent<Collider> ().enabled = visible;
		GetComponent<Renderer> ().enabled = visible;
	}

	void CheckState() {
		switch (State) {
		case AIState.Waiting:
			if (TargetPoint.Team == TeamColor.Player) {
				State = AIState.Ready;
				SetVisible (true);
			}
			break;
		case AIState.Ready:
			GatherTroops ();
			break;
		case AIState.Traveling:
			AttackClosestBuilding ();
			break;
		case AIState.Attacking:
			//all is done in FixedUpdate
			if (Troops.Count == 0) {
				State = AIState.Returning;
			}
			break;
		case AIState.Capturing:
			if (TargetPoint.Team == Team) {
				Debug.Log ("My job here is done!");
				GameManager.Current.Build (transform.position - Vector3.left*4, BuildingType.Tower);
				State = AIState.Returning;
			}
			break;
		case AIState.Returning:
			//all is done in FixedUpdate
			break;
		default:
			break;
		}
	}

	void RetireTroop(UnitScript troop) {
		Troops.Remove (troop);
		troop.UActionDeath -= RetireTroop;
	}

	void GatherTroops() {
		foreach (var troop in NearbyUnits()) {
			if (troop.Team == Team) {
				Troops.Add (troop);
				troop.UActionDeath += RetireTroop;
				troop.Follow (transform);
			}
		}
		if (Troops.Count > 0) {
			State = AIState.Traveling;
		}
	}

	void AttackClosestBuilding() {
		if (Troops.Count > 0) {
			var target = ClosestBuilding ();
			if (target != null) {
				foreach (var troop in Troops) {
					troop.Stay (target);
				}
				State = AIState.Attacking;
				TargetBuilding = target;
			} else {
				foreach (var troop in Troops) {
					troop.Follow (transform);
				}
				State = AIState.Traveling;
			}
		} else {
			State = AIState.Returning;
		}
	}

	void FixedUpdate() {
		if (State == AIState.Traveling) {
			transform.position = Vector3.MoveTowards (transform.position, TargetPoint.transform.position, Time.fixedDeltaTime * Speed);
			if (transform.position == TargetPoint.transform.position) {
				if (!TargetCluster.HasBuildings) {
					State = AIState.Capturing;
				} else { //in case we cross point or player builds a building
					Debug.Log ("Stuck or new building was built!");
					AttackClosestBuilding ();
				}
			}
		} else if (State == AIState.Attacking) {
			if (TargetBuilding == null) {
				AttackClosestBuilding ();
			} else {
				transform.position = Vector3.MoveTowards (transform.position, TargetBuilding.position, Time.fixedDeltaTime * Speed);
			}
		} else if (State == AIState.Returning) {
			transform.position = Vector3.MoveTowards (transform.position, StartingPoint, Time.fixedDeltaTime * Speed);
			if (transform.position == StartingPoint) {
				SetVisible (false);
				State = AIState.Waiting;
				for (int i = Troops.Count-1; i >= 0; i--) {
					Troops [i].OnDeath ();
				}
			}
		}
	}
}
