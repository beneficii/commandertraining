﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClusterScript : MonoBehaviour {

	public TeamColor Team;

	public BuildingBase[] Buildings {
		get { return GetComponentsInChildren<BuildingBase> ();}
	}

	public CapturePoint Point {
		get { return GetComponentInChildren<CapturePoint> ();}
	}

	public bool HasBuildings {
		get { return transform.childCount > 1;}
	}

	void Start() {
		foreach (var building in Buildings) {
			building.UpdateTeam (Team);
		}
	}
}
