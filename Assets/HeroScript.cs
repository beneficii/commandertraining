﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class HeroScript : MonoBehaviour {

	public TeamColor Team;
	public float CommandRadius = 10f;
	[HideInInspector]public ClusterScript CapturingCluster = null; //prob should be bool

	void Start() {
		if (Team == TeamColor.Player) {
			InvokeRepeating ("RefreshAttack", 5f, 1f);
		}
	}

	void RefreshAttack() {
		Transform target = ClosestBuilding ();
		if (target != null) {
			foreach (var unit in NearbyUnits()) {
				if (unit.Team == Team && unit.State != UnitState.Attacking) {
					unit.Stay (target);
				}
			}
		}
	}

	public IEnumerable<UnitScript> NearbyUnits() {
		foreach (var unitObj in Physics.OverlapSphere(transform.position, CommandRadius, LayerMask.GetMask("Units"))) {
			yield return unitObj.GetComponent<UnitScript> ();
		}
	}

	public Transform ClosestBuilding() {
		float distance = 999999999;
		Transform target = null;
		foreach(var building in Physics.OverlapSphere (transform.position, CommandRadius, LayerMask.GetMask ("Buildings"))) {
			if(building.GetComponent<BuildingBase>().Team != Team) {
				float d = Vector3.Distance (transform.position, building.transform.position);
				if (d < distance) {
					distance = d;
					target = building.transform;
				}
			}
		}
		return target;
	}

	public void Shout(UnitState command) {
		foreach (var unit in NearbyUnits()) {
			if (unit.Team == Team) {
				switch (command) {
				case UnitState.Ready:
					unit.Stay (ClosestBuilding());
					break;
				case UnitState.Following:
					unit.Follow (transform);
					break;
				}
			}
		}
	}
}
