﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerScript : BuildingBase {

	public GameObject BulletPrefab;
	public Transform BulletSpawn;
	public TriggerArea AttackArea;
	float AttackRadius = 6f;


	public override void UpdateTeam (TeamColor team)
	{
		base.UpdateTeam (team);
		AttackArea.GetComponent<Renderer> ().material = TMaterials.Transparent;
		BulletSpawn.GetComponent<Renderer>().material = TMaterials.Transparent;
		CancelInvoke ();
		Invoke ("ActivateArea", 5f);
		InvokeRepeating ("Attack", 5f, 0.5f);
	}

	void ActivateArea() {
		BulletSpawn.GetComponent<Renderer>().material = TMaterials.Normal;
	}

	void Attack() {
		//foreach (var unit in AttackArea.Units) {
		foreach (var unitObj in Physics.OverlapSphere(transform.position, AttackRadius, LayerMask.GetMask("Units"))) {
			UnitScript unit = unitObj.GetComponent<UnitScript> ();
			if (unit.Team != Team) {
				Instantiate (BulletPrefab, BulletSpawn.position, Quaternion.identity).GetComponent<BulletScript> ().target = unit;
				break;
			}
		}
	}
}
