﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[System.Serializable]
public class TeamMaterials {
	public Material Normal;
	public Material Transparent;
}

public enum TeamColor: int {
	Player = 0,
	Enemy = 1,
	Neutral =2,
	None = 99
}

public enum BuildingType {
	Barracks = 0,
	Tower = 1,
	Generator = 2,
	None = 99
}

public class GameManager : MonoBehaviour {

	int gold;
	public int Gold {
		get{ return gold; }
		set{
			gold = value;
			GoldText.text = gold.ToString ();
		}
	}
	public Text GoldText;

	public GameObject BuildMenu;
	public GameObject Player;
	public Text DialogueText;

	static public GameManager Current;

	public TeamMaterials[] TeamMaterials;
	public GameObject[] BuildingPrefabs;

	// Use this for initialization
	void Awake () {
		Current = this;
		Gold = 999;
		Player = GameObject.Find ("RigidBodyFPSController");
		DOTween.Init();
	}

	public void OpenBuildMenu() {
		Player.GetComponent<UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController> ().mouseLook.SetCursorLock (false);
		Player.GetComponent<UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController> ().enabled = false;
		BuildMenu.SetActive (true);
	}

	public void CloseBuildMenu(BuildingType building) {
		Player.GetComponent<UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController> ().enabled = true;
		Player.GetComponent<UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController> ().mouseLook.SetCursorLock (true);
		Player.GetComponent<PlayerScript> ().building = building;
		BuildMenu.SetActive (false);
	}

	public void CloseBuildMenu() {
		CloseBuildMenu (BuildingType.None);
	}

	public void Build(Vector3 pos, BuildingType building) {
		PlayerScript player = Player.GetComponent<PlayerScript> ();
		pos.y = 0.5f;
		player.BuilderBlock.gameObject.SetActive (false);
		GameObject prefab = BuildingPrefabs [(int)building];
		Instantiate (prefab, pos, prefab.transform.rotation);
		Gold -= prefab.GetComponent<BuildingBase>().Cost;
	}

	public void SetDialogue(string message) {
		DialogueText.text = message;
	}
}
