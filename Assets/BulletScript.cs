﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	const float Speed = 25f;

	public UnitScript target;

	void FixedUpdate() {
		if (target.Alive) {
			transform.position = Vector3.MoveTowards (transform.position, target.transform.position, Time.fixedDeltaTime*Speed);
			if (transform.position == target.transform.position) {
				target.OnDeath ();
				Destroy (gameObject);
			}
		} else {
			Destroy (gameObject);
		}
	}
}
