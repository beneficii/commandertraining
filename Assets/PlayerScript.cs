﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

	[HideInInspector]public BuildingType building = BuildingType.None;
	public Transform BuilderBlock;
	const float TileSize = 2f;

	LayerMask BuildAreaMask;

	void Awake() {
		BuildAreaMask = LayerMask.GetMask ("BuildArea", "Buildings", "BuildObstacles");
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.E)) {
			GetComponent<HeroScript> ().Shout (UnitState.Following);
		} else if (Input.GetKeyDown (KeyCode.Q)) {
			GetComponent<HeroScript> ().Shout (UnitState.Ready);
		} else if (Input.GetMouseButtonDown (0)) {
			if (!BuilderBlock.gameObject.activeSelf) {
				GameManager.Current.OpenBuildMenu ();
			} else {
				GameManager.Current.Build (BuilderBlock.position, building);
				building = BuildingType.None;
			}

		} else if (Input.GetMouseButtonDown (1)) {
			if (building != BuildingType.None) {
				BuilderBlock.gameObject.SetActive (false);
				building = BuildingType.None;
			} else if(GameManager.Current.BuildMenu.activeSelf){
				GameManager.Current.CloseBuildMenu ();
			}

		}
		if (building != BuildingType.None) {
			RaycastHit hit;
			if (Physics.Raycast (Camera.main.ViewportPointToRay (new Vector2 (0.5f, 0.5f)), out hit, 10, BuildAreaMask, QueryTriggerInteraction.Collide) &&
			    hit.transform.gameObject.layer == LayerMask.NameToLayer ("BuildArea") && hit.transform.GetComponentInParent<CapturePoint> ().Team == TeamColor.Player) {
				if (!BuilderBlock.gameObject.activeSelf) {
					BuilderBlock.gameObject.SetActive (true);
				}
				BuilderBlock.position = new Vector3 (Mathf.RoundToInt (hit.point.x / TileSize) * TileSize, 0, Mathf.RoundToInt (hit.point.z / TileSize) * TileSize);//hit.point;
			}
		}
	}
}
