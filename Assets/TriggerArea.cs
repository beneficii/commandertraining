﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerArea : MonoBehaviour {

	public List<BuildingBase> Buildings;
	public List<UnitScript> Units;


	void Awake() {
		Buildings = new List<BuildingBase> ();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Building")) {
			Buildings.Add (other.GetComponent<BuildingBase> ());
		} else if (other.CompareTag ("Unit")) {
			Units.Add (other.GetComponent<UnitScript> ());
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.CompareTag ("Building")) {
			Buildings.Remove (other.GetComponent<BuildingBase> ());
		} else if (other.CompareTag ("Unit")) {
			Units.Remove (other.GetComponent<UnitScript>());
		}
	}
}
