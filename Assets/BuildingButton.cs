﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingButton : MonoBehaviour {

	public BuildingType building;
	public int Cost;

	public Text CostText;
	public Text Label;

	void Update() {
		GetComponent<Button> ().interactable = GameManager.Current.Gold >= Cost;
	}

	void Start () {
		Cost = GameManager.Current.BuildingPrefabs[(int)building].GetComponent<BuildingBase>().Cost;
		Label.text = building.ToString ();
		CostText.text = Cost.ToString();
	}
	
	public void OnClick () {
		GameManager.Current.CloseBuildMenu (building);
	}
}
