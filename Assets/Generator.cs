﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Generator : BuildingBase {

	public Transform Coin;

	public override void UpdateTeam (TeamColor team)
	{
		base.UpdateTeam (team);
		CancelInvoke ();
		InvokeRepeating ("GainResource", 1f, 2f);
	}


	void GainResource () {
		if (Team == TeamColor.Player) {
			Coin.DOLocalMoveY (1.5f, 0.4f)
				.SetEase(Ease.InOutCubic)
				.OnComplete(()=>{
					GameManager.Current.Gold++;
					Coin.localPosition = Vector3.zero;
				});
		}
	}
}
