﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitState {
	None = 0,
	Ready = 1,
	Following = 2,
	Attacking = 3
}

public class UnitScript : MonoBehaviour {

	public bool Alive = false;
	public delegate void UAction(UnitScript unit);

	public UAction UActionDeath;

	public GameObject[] Emotions;

	Transform Target = null;
	float MinDistance = 5f;
	float Speed = 4f;

	Rigidbody rb;

	UnitState state;
	public UnitState State {
		get { return state;}
		set {
			if (Alive) {
				state = value;
				for (int i = 0; i < Emotions.Length; i++) {
					Emotions [i].SetActive (i == (int)state);
				}
				rb.isKinematic = (value == UnitState.Ready);
			}
		}
	}

	TeamColor team = TeamColor.None;
	public TeamColor Team {
		get { return team;}
		set {
			team = value;
		}
	}

	void Awake() {
		rb = GetComponent<Rigidbody> ();
	}

	public void Revive() {
		Alive = true;
		//GetComponent<Collider> ().enabled = true;
	}

	public void Follow(Transform target) {
		Target = target;
		State = UnitState.Following;
	}

	public void Stay(Transform target) {
		Target = target;
		if (target == null) {
			State = UnitState.Ready;
		} else {
			State = UnitState.Attacking;
		}
	}

	public void OnDeath() {
		State = UnitState.None;
		Alive = false;

		//GetComponent<Collider> ().enabled = false;
		UActionDeath.Invoke(this);
	}

	void FixedUpdate() {
		if ((state == UnitState.Following && Vector3.Distance (transform.position, Target.position) > MinDistance) || state == UnitState.Attacking) {
			if (Target == null) {
				State = UnitState.Ready;
			} else {
				rb.velocity = Vector3.zero;
				transform.position = Vector3.MoveTowards (transform.position, Target.position, Time.fixedDeltaTime * Speed);
				//transform.position = Vector3.MoveTowards (transform.position, new Vector3(Target.position.x, transform.position.y, Target.position.z), Time.fixedDeltaTime * Speed);
			}
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.CompareTag ("Building") && collision.collider.GetComponent<BuildingBase> ().Team != Team) {
			Destroy (collision.collider.gameObject);
			OnDeath ();
		}
	}
}
